//
//  DetailInteractorInterface.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

protocol DetailInteractorInterface: class {
    var ownerAvatarUrl: URL? { get }
    var ownerName: String { get }
    var repositoryName: String { get }
    var repositoryDescription: String { get }
    var repositoryUrl: URL { get }
    var numberOfStars: Int { get }
    var numberOfForks: Int { get }
    var numberOfOpenIssues: Int { get }
}
