//
//  DetailInteractor.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

class DetailInteractor: DetailInteractorInterface {

    
    // MARK: - Variables
    var repository: Repository
    
    
    // MARK: - Initializer
    init(repository: Repository) {
        self.repository = repository
    }
    
    
    // MARK: - DetailInteractorInterface
    var ownerAvatarUrl: URL? {
        return repository.owner.avatarUrl
    }
    
    
    var ownerName: String {
        return repository.owner.login
    }
    
    
    var repositoryName: String {
        return repository.name
    }
    
    
    var repositoryDescription: String {
        return repository.description
    }
    
    
    var repositoryUrl: URL {
        return repository.url
    }
    
    
    var numberOfStars: Int {
        return repository.starsCount
    }
    
    
    var numberOfForks: Int {
        return repository.forksCount
    }
    
    
    var numberOfOpenIssues: Int {
        return repository.openIssuesCount
    }
}
