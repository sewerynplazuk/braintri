//
//  DetailViewInterface.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

protocol DetailViewInterface: class {
    func setupSubviews()
    func setupConstraints()
    func setRootViewBackgroundColor(_ backgroundColor: UIColor)
    func setOwnerImageViewImageFromUrl(_ url: URL)
    func setOwnerLabelText(_ text: String)
    func setRepositoryNameLabelText(_ text: String)
    func setStarsCountLabelText(_ text: String)
    func setForksCountLabelText(_ text: String)
    func setOpenIssuesLabelText(_ text: String)
    func setDescriptionLabelText(_ text: String)
    func setOpenUrlButtonTitle(_ title: String)
}
