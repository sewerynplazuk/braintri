//
//  DetailViewController.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController, DetailViewInterface {
    
    // MARK: - Variables
    var presenter: DetailModuleInterface!
    // Subviews
    var ownerImageView: UIImageView!
    var ownerLabel: UILabel!
    var repositoryNameLabel: UILabel!
    var starsCountLabel: UILabel!
    var forksCountLabel: UILabel!
    var openIssuesLabel: UILabel!
    var descriptionLabel: UILabel!
    var openUrlButton: UIButton!
    
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.onViewDidLoad()
    }
    
    
    // MARK: - Buttons targets
    @objc private func tappedOpenUrlButton() {
        self.presenter.onOpenUrlButtonTap()
    }
    
    
    // MARK: - DetailViewInterface
    func setupSubviews() {
        self.ownerImageView = UIImageView()
        self.ownerImageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(ownerImageView)
        
        self.ownerLabel = UILabel()
        self.ownerLabel.numberOfLines = 2
        self.ownerLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(ownerLabel)
        
        self.repositoryNameLabel = UILabel()
        self.repositoryNameLabel.textAlignment = .center
        self.repositoryNameLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(repositoryNameLabel)
        
        self.starsCountLabel = UILabel()
        self.starsCountLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(starsCountLabel)

        self.forksCountLabel = UILabel()
        self.forksCountLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(forksCountLabel)

        self.openIssuesLabel = UILabel()
        self.openIssuesLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(openIssuesLabel)

        self.descriptionLabel = UILabel()
        self.descriptionLabel.numberOfLines = 0
        self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(descriptionLabel)
        
        self.openUrlButton = UIButton()
        self.openUrlButton.backgroundColor = .black
        self.openUrlButton.setTitleColor(.white, for: .normal)
        self.openUrlButton.addTarget(self, action: #selector(self.tappedOpenUrlButton), for: .touchUpInside)
        self.openUrlButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(openUrlButton)
    }
    
    
    func setupConstraints() {
        self.ownerImageView.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        self.ownerImageView.widthAnchor.constraint(equalToConstant: 100.0).isActive = true
        self.ownerImageView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20.0).isActive = true
        self.ownerImageView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 20.0).isActive = true
        
        self.ownerLabel.topAnchor.constraint(equalTo: ownerImageView.topAnchor).isActive = true
        self.ownerLabel.leftAnchor.constraint(equalTo: ownerImageView.rightAnchor, constant: 20.0).isActive = true
        self.ownerLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20.0).isActive = true
        self.ownerLabel.bottomAnchor.constraint(lessThanOrEqualTo: ownerImageView.bottomAnchor).isActive = true
        
        self.repositoryNameLabel.topAnchor.constraint(equalTo: ownerImageView.bottomAnchor, constant: 20.0).isActive = true
        self.addHorizontalMarginsConstraintsForView(repositoryNameLabel, marginWidth: 20.0)
        
        self.starsCountLabel.topAnchor.constraint(equalTo: repositoryNameLabel.bottomAnchor, constant: 20.0).isActive = true
        self.addHorizontalMarginsConstraintsForView(starsCountLabel, marginWidth: 20.0)
        
        self.forksCountLabel.topAnchor.constraint(equalTo: starsCountLabel.bottomAnchor, constant: 8.0).isActive = true
        self.addHorizontalMarginsConstraintsForView(forksCountLabel, marginWidth: 20.0)

        self.openIssuesLabel.topAnchor.constraint(equalTo: forksCountLabel.bottomAnchor, constant: 8.0).isActive = true
        self.addHorizontalMarginsConstraintsForView(openIssuesLabel, marginWidth: 20.0)
        
        self.descriptionLabel.topAnchor.constraint(equalTo: openIssuesLabel.bottomAnchor, constant: 20.0).isActive = true
        self.addHorizontalMarginsConstraintsForView(descriptionLabel, marginWidth: 20.0)

        self.openUrlButton.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
        self.addHorizontalMarginsConstraintsForView(openUrlButton, marginWidth: 20.0)
        self.openUrlButton.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        self.openUrlButton.topAnchor.constraint(greaterThanOrEqualTo: descriptionLabel.bottomAnchor, constant: 20.0).isActive = true
    }
    

    func setOwnerImageViewImageFromUrl(_ url: URL) {
        self.ownerImageView.kf.setImage(with: url)
    }
    
    
    func setOwnerLabelText(_ text: String) {
        self.ownerLabel.text = text
    }
    
    
    func setRepositoryNameLabelText(_ text: String) {
        self.repositoryNameLabel.text = text
    }
    
    
    func setStarsCountLabelText(_ text: String) {
        self.starsCountLabel.text = text
    }
    
    
    func setForksCountLabelText(_ text: String) {
        self.forksCountLabel.text = text
    }
    
    
    func setOpenIssuesLabelText(_ text: String) {
        self.openIssuesLabel.text = text
    }
    
    
    func setDescriptionLabelText(_ text: String) {
        self.descriptionLabel.text = text
    }
    
    
    func setOpenUrlButtonTitle(_ title: String) {
        self.openUrlButton.setTitle(title, for: .normal)
    }
}
