//
//  DetailPresenter.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

class DetailPresenter: DetailModuleInterface {
    
    // MARK: - Variables
    weak var view: DetailViewInterface!
    var interactor: DetailInteractorInterface!
    var wireframe: DetailWireframeInterface!
    
    
    // MARK: - DetailModuleInterface
    func onViewDidLoad() {
        self.view.setupSubviews()
        self.view.setupConstraints()
        self.view.setRootViewBackgroundColor(.white)
        
        if let ownerAvatarUrl = interactor.ownerAvatarUrl {
            self.view.setOwnerImageViewImageFromUrl(ownerAvatarUrl)
        }
        
        self.view.setOwnerLabelText("Repository owned\nby \(interactor.ownerName)")
        self.view.setRepositoryNameLabelText(interactor.repositoryName.uppercased())
        self.view.setStarsCountLabelText("Stars: \(interactor.numberOfStars)")
        self.view.setForksCountLabelText("Forks: \(interactor.numberOfForks)")
        self.view.setOpenIssuesLabelText("Open issues: \(interactor.numberOfOpenIssues)")
        self.view.setDescriptionLabelText("Description:\n\(interactor.repositoryDescription)")
        self.view.setOpenUrlButtonTitle("Go to repository")
    }
    
    
    func onOpenUrlButtonTap() {
        self.wireframe.openUrl(interactor.repositoryUrl)
    }
}
