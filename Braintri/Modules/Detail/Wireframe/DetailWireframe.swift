//
//  DetailWireframe.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class DetailWireframe: DetailWireframeInterface {
    
    // MARK: - Variables
    weak var detailViewController: DetailViewController!
    
    
    // MARK: - DetailWireframeInterface
    @discardableResult
    func openUrl(_ url: URL) -> Bool {
        guard UIApplication.shared.canOpenURL(url) else { return false }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        
        return true
    }
}
