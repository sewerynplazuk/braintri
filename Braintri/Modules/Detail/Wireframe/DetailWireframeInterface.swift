//
//  DetailWireframeInterface.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

protocol DetailWireframeInterface: class {
    @discardableResult
    func openUrl(_ url: URL) -> Bool
}
