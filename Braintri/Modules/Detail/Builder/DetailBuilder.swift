//
//  DetailBuilder.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

class DetailBuilder {
    
    // MARK: - Class lifecycle
    private init() {}
    
    
    // MARK: - Control
    static func build(repository: Repository) -> DetailViewController {
        let view = DetailViewController()
        let interactor = DetailInteractor(repository: repository)
        let presenter = DetailPresenter()
        let wireframe = DetailWireframe()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        
        wireframe.detailViewController = view
        
        return view
    }
    
}
