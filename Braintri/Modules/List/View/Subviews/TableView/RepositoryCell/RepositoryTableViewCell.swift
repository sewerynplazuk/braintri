//
//  RepositoryTableViewCell.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    
    // MARK: - Constants
    static let identifier: String = "RepositoryTableViewCell"
    
    
    // MARK: - Variables
    var dataSource: RepositoryTableViewCellDataSource?
}
