//
//  ListViewInterface.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

protocol ListViewInterface: class {
    func setupSubviews()
    func setupConstraints()
    func setRootViewBackgroundColor(_ backgroundColor: UIColor)
    func setTableViewDataSource(_ dataSource: UITableViewDataSource)
    func setTableViewDelegate(_ delegate: UITableViewDelegate)
    func reloadDataInTableView()
}
