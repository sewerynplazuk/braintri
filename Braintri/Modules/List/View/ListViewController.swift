//
//  ListViewController.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, ListViewInterface {
    
    // MARK: - Variables
    var presenter: ListModuleInterface!
    // Subviews
    var searchBar: UISearchBar!
    var tableView: UITableView!
    
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.onViewDidLoad()
    }
    
    
    // MARK: - ListViewInterface
    func setupSubviews() {
        self.searchBar = UISearchBar()
        self.searchBar.translatesAutoresizingMaskIntoConstraints = false
        self.searchBar.delegate = self
        self.view.addSubview(searchBar)
        
        self.tableView = UITableView()
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
    }
    
    
    func setupConstraints() {
        searchBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        searchBar.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        searchBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        searchBar.bottomAnchor.constraint(equalTo: tableView.topAnchor).isActive = true
        
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    
    func setTableViewDataSource(_ dataSource: UITableViewDataSource) {
        self.tableView.dataSource = dataSource
    }
    
    
    func setTableViewDelegate(_ delegate: UITableViewDelegate) {
        self.tableView.delegate = delegate
    }
    
    
    func reloadDataInTableView() {
        self.tableView.reloadData()
    }
}
