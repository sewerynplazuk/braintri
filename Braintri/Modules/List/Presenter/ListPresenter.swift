//
//  ListPresenter.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import PromiseKit

class ListPresenter: ListModuleInterface {
    
    // MARK: - Variables
    weak var view: ListViewInterface!
    var interactor: ListInteractorInterface!
    var wireframe: ListWireframeInterface!
    
    
    // MARK: - ListModuleInterface
    func onViewDidLoad() {
        self.view.setupSubviews()
        self.view.setupConstraints()
        self.view.setRootViewBackgroundColor(.white)
        
        self.view.setTableViewDataSource(interactor.tableViewDataSource)
        self.view.setTableViewDelegate(interactor.tableViewDelegate)
        
        self.registerForWillDisplayLastCellCallback()
        self.registerForDidSelectRowCallback()
    }
    
    
    func onSearchBarSearchButtonClick(searchText: String?) {
        guard let searchText = searchText, !searchText.isEmpty else {
            self.clearTableView()
            return
        }
        
        self.interactor.searchRepositories(searchText).done { [weak self] in
            self?.view.reloadDataInTableView()
        }.catch { error in
            print("Searching repositories for query \(searchText) failed with error \(error.localizedDescription)")
        }
    }
    
    
    func onSearchBarDidBeginEditing() {
        self.clearTableView()
    }
    
    
    // MARK: - Helpers
    private func registerForWillDisplayLastCellCallback() {
        self.interactor.registerForWillDisplayLastCellCallback() { [weak self] in
            self?.interactor.fetchNextPageOfRepositories().done { [weak self] in
                self?.view.reloadDataInTableView()
            }.catch { error in
                print("Fetching next page of repositories failed with error \(error.localizedDescription)")
            }
        }
    }
    
    
    private func registerForDidSelectRowCallback() {
        self.interactor.registerForDidSelectRowCallback { [weak self] row in
            guard let repository = self?.interactor.getRepositoryOnRow(row) else { return }
            self?.wireframe.pushDetailModuleForSelectedRepository(repository)
        }
    }
    
    
    private func clearTableView() {
        self.interactor.abortSearchRequest()
        self.interactor.clearTableViewDataSource()
        self.view.reloadDataInTableView()
    }
}
