//
//  ListInteractor.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import PromiseKit

class ListInteractor: ListInteractorInterface {
    
    // MARK: - Variables
    private lazy var listNetworking = ListNetworking()
    
    
    // MARK: - Initializer
    init() {
        self.tableViewDataSource = ListTableViewDataSource()
        self.tableViewDelegate = ListTableViewDelegate()
    }

    
    // MARK: - ListInteractorInterface
    private(set) var tableViewDataSource: ListTableViewDataSource
    private(set) var tableViewDelegate: ListTableViewDelegate

    
    func searchRepositories(_ searchQuery: String) -> Promise<Void> {
        return listNetworking.searchRepositories(searchQuery).done { [weak self] repositories in
            self?.tableViewDataSource.setRepositories(repositories)
        }
    }
    
    
    func fetchNextPageOfRepositories() -> Promise<Void> {
        return listNetworking.fetchNextPageOfRepositories().done { [weak self] repositories in
            self?.tableViewDataSource.appendRepositories(repositories)
        }
    }
    
    
    func abortSearchRequest() {
        self.listNetworking.abortSearchRequest()
    }
    
    
    func clearTableViewDataSource() {
        self.tableViewDataSource.removeAllRepositories()
    }
    
    
    func getRepositoryOnRow(_ row: Int) -> Repository {
        return tableViewDataSource.repositories[row]
    }
    
    
    func registerForWillDisplayLastCellCallback(_ callback: @escaping () -> Void) {
        self.tableViewDelegate.willDisplayLastCell = callback
    }
    
    
    func registerForDidSelectRowCallback(_ callback: @escaping (_ row: Int) -> Void) {
        self.tableViewDelegate.didSelectRow = callback
    }
}
