//
//  ListInteractorInterface.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import PromiseKit

protocol ListInteractorInterface: class {
    var tableViewDataSource: ListTableViewDataSource { get }
    var tableViewDelegate: ListTableViewDelegate { get }

    func searchRepositories(_ searchQuery: String) -> Promise<Void>
    func fetchNextPageOfRepositories() -> Promise<Void>
    
    func abortSearchRequest()
    func clearTableViewDataSource()
    func getRepositoryOnRow(_ row: Int) -> Repository
    
    func registerForWillDisplayLastCellCallback(_ callback: @escaping () -> Void)
    func registerForDidSelectRowCallback(_ callback: @escaping (_ row: Int) -> Void)
}
