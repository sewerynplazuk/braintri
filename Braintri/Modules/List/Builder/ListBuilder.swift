//
//  ListBuilder.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class ListBuilder {
    
    // MARK: - Class lifecycle
    private init() {}
    
    
    // MARK: - Control
    static func build(embedInNavigationController: Bool) -> UIViewController {
        let view = ListViewController()
        let interactor = ListInteractor()
        let presenter = ListPresenter()
        let wireframe = ListWireframe()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        
        wireframe.listViewController = view
        
        if embedInNavigationController {
            let navigationController = UINavigationController(rootViewController: view)
            return navigationController
        } else {
            return view
        }
    }
    
}
