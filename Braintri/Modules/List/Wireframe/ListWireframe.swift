//
//  ListWireframe.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

class ListWireframe: ListWireframeInterface {
    
    // MARK: - Variables
    weak var listViewController: ListViewController!
    
    
    // MARK: - ListWireframeInterface
    func pushDetailModuleForSelectedRepository(_ repository: Repository) {
        let detailViewController = DetailBuilder.build(repository: repository)
        self.listViewController.navigationController?.pushViewController(detailViewController, animated: true)
    }
}
