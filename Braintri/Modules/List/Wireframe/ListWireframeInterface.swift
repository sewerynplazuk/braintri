//
//  ListWireframeInterface.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

protocol ListWireframeInterface: class {
    func pushDetailModuleForSelectedRepository(_ repository: Repository)
}
