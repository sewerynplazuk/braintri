//
//  ListTableViewDataSource.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class ListTableViewDataSource: NSObject, UITableViewDataSource {
    
    // MARK: - Variables
    private(set) var repositories: [Repository] = []
    
    
    // MARK: - Lifecycle
    func setRepositories(_ repositories: [Repository]) {
        self.repositories = repositories
    }
    
    
    func appendRepositories(_ repositories: [Repository]) {
        self.repositories.append(contentsOf: repositories)
    }
    
    
    func removeAllRepositories() {
        self.repositories.removeAll()
    }
    
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(RepositoryTableViewCell.self, forCellReuseIdentifier: RepositoryTableViewCell.identifier)
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryTableViewCell.identifier, for: indexPath) as? RepositoryTableViewCell else { fatalError() }
        cell.selectionStyle = .none
        
        let repository = self.repositories[indexPath.row]
        let cellDataSource = RepositoryTableViewCellDataSource(cell: cell, repository: repository)
        cell.dataSource = cellDataSource
        
        return cell
    }
}
