//
//  ListTableViewDelegate.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class ListTableViewDelegate: NSObject, UITableViewDelegate {
    
    // MARK: - Variables
    var willDisplayLastCell: (() -> Void)?
    var didSelectRow: ((Int) -> Void)?
    
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let numberOfRows = tableView.numberOfRows(inSection: indexPath.section)
        
        if indexPath.row == numberOfRows - 1 {
            guard let willDisplayLastCell = self.willDisplayLastCell else { return }
            willDisplayLastCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let didSelectRow = self.didSelectRow else { return }
        didSelectRow(indexPath.row)
    }
}
