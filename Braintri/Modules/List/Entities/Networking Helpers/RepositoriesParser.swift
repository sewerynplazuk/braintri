//
//  RepositoriesParser.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import Unbox

class RepositoriesParser {
    
    // MARK: - Variables
    var dictionariesArray: [[String:Any]]
    
    
    // MARK: - Initializer
    init(dictionariesArray: [[String:Any]]) {
        self.dictionariesArray = dictionariesArray
    }
    
    
    // MARK: - Parsing
    func parse() -> [Repository] {
        var output: [Repository] = []
        for dictionary in dictionariesArray {
            let unboxer = Unboxer(dictionary: dictionary)
            
            guard let repository = try? Repository(unboxer: unboxer) else {
                continue
            }
            
            output.append(repository)
        }
        
        return output
    }
}
