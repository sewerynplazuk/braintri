//
//  ListNetworking.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import Unbox
import PromiseKit

class ListNetworking {
    
    // MARK: - Variables
    private var lastSearchQuery: String?
    private var lastFetchedPage: Int = 1
    private var searchRequest: Request!
    private lazy var fetchNextPageRequests: [Int:Request] = [:]
    private lazy var fetchNextPagePromiseQueue = Promise()
    
    
    // MARK: - Control
    func searchRepositories(_ searchQuery: String) -> Promise<[Repository]> {
        self.lastSearchQuery = searchQuery
        self.lastFetchedPage = 1
        self.searchRequest = Request(route: .searchRepositories(searchQuery: searchQuery, page: nil))
        return searchRequest.getAll().then { dictionariesArray -> Promise<[Repository]> in
            let repositoriesParser = RepositoriesParser(dictionariesArray: dictionariesArray)
            return .value(repositoriesParser.parse())
        }.ensure { [weak self] in
            self?.searchRequest = nil
        }
    }
    
    
    func abortSearchRequest() {
        self.searchRequest = nil
    }
    
    
    func fetchNextPageOfRepositories() -> Promise<[Repository]> {
        guard let lastSearchQuery = self.lastSearchQuery else {
            let error = ErrorParser().getErrorWithLocalizedDescription("You have to search repositories first before you can fetch next page")
            return Promise(error: error)
        }
        
        let thisPage = lastFetchedPage + 1
        
        let request = Request(route: .searchRepositories(searchQuery: lastSearchQuery, page: thisPage))
        self.fetchNextPageRequests[thisPage] = request
        
        return fetchNextPagePromiseQueue.then { _ -> Promise<[Repository]> in
            return request.getAll().then { [weak self] dictionariesArray -> Promise<[Repository]> in
                self?.lastFetchedPage += 1
                
                let repositoriesParser = RepositoriesParser(dictionariesArray: dictionariesArray)
                return .value(repositoriesParser.parse())
            }.ensure { [weak self] in
                self?.fetchNextPageRequests.removeValue(forKey: thisPage)
            }
        }
    }
}
