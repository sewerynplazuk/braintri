//
//  RepositoryTableViewCellDataSource.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

class RepositoryTableViewCellDataSource {
    
    // MARK: - Variables
    private weak var cell: RepositoryTableViewCell?
    private var repository: Repository
    
    
    // MARK: - Initializer
    init(cell: RepositoryTableViewCell, repository: Repository) {
        self.cell = cell
        self.repository = repository
        self.populateCell()
    }
    
    
    // MARK: - Setup subviews
    private func populateCell() {
        self.cell?.imageView?.kf.setImage(with: repository.owner.avatarUrl)
        self.cell?.textLabel?.text = repository.fullName
        self.cell?.detailTextLabel?.text = repository.description
    }
}
