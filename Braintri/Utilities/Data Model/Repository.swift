//
//  Repository.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import Unbox

struct Repository: Unboxable {
    
    // MARK: - Variables
    var id: Int
    var name: String
    var fullName: String
    var description: String
    var starsCount: Int
    var forksCount: Int
    var openIssuesCount: Int
    var url: URL
    var owner: Owner
    
    
    // MARK: - Unboxable
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.name = try unboxer.unbox(key: "name")
        self.fullName = try unboxer.unbox(key: "full_name")
        self.description = try unboxer.unbox(key: "description")
        self.starsCount = try unboxer.unbox(key: "stargazers_count")
        self.forksCount = try unboxer.unbox(key: "forks_count")
        self.openIssuesCount = try unboxer.unbox(key: "open_issues_count")
        self.url = try unboxer.unbox(key: "html_url")
        self.owner = try unboxer.unbox(key: "owner")
    }
}
