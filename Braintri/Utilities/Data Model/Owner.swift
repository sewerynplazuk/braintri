//
//  Owner.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import Unbox

struct Owner: Unboxable {
    
    // MARK: - Variables
    var id: Int
    var login: String
    var avatarUrl: URL?
    
    
    // MARK: - Unboxable
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.login = try unboxer.unbox(key: "login")
        self.avatarUrl = unboxer.unbox(key: "avatar_url")
    }
}
