//
//  Request.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import PromiseKit


class Request: Requestable {
    
    // MARK: - Variables
    var route: RequestRoute
    private(set) var urlSession: URLSession
    
    
    // MARK: - Initiazlier
    init(route: RequestRoute) {
        self.route = route
        self.urlSession = URLSession(configuration: .default, delegate: nil, delegateQueue: nil)
    }
    
    
    // MARK: - Requests
    func getAll() -> Promise<[[String : Any]]> {
        return firstly {
            self.executeGetRequest()
        }.then { responseData -> Promise<[[String : Any]]> in
            do {
                let decodedDictionary = try DataDecoder(data: responseData).decodeDataToArray()
                return .value(decodedDictionary)
            } catch {
                return Promise(error: error)
            }
        }
    }
    
    
    // MARK: - Helpers
    private func executeGetRequest() -> Promise<Data> {
        var urlRequest = URLRequest(url: route.url)
        self.configureGetRequest(&urlRequest)
        
        return Promise { seal in
            self.urlSession.dataTask(with: urlRequest) { [weak self] (data, response, error) in
                if let error = error {
                    seal.reject(error)
                } else {
                    let errorParser = ErrorParser()
                    
                    guard let self = self else {
                        let deallocationError = errorParser.getErrorWithLocalizedDescription("Request object has been deallocated")
                        seal.reject(deallocationError)
                        return
                    }
                    
                    guard let httpResponse = response as? HTTPURLResponse else {
                        assertionFailure()
                        return
                    }
                    
                    guard self.validateStatusCode(httpResponse.statusCode) else {
                        let statusCodeError = errorParser.getErrorWithLocalizedDescription("Request failed with status code: \(httpResponse.statusCode)")
                        seal.reject(statusCodeError)
                        return
                    }
                    
                    guard let data = data else {
                        let dataError = errorParser.getErrorWithLocalizedDescription("Request returned nil data")
                        seal.reject(dataError)
                        return
                    }
                    
                    seal.fulfill(data)
                }
            }.resume()
        }
    }
}
