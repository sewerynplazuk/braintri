//
//  DataDecoder.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation


class DataDecoder {
    
    // MARK: - Variables
    private(set) var data: Data
    private lazy var errorParser = ErrorParser()
    
    
    // MARK: - Initializer
    init(data: Data) {
        self.data = data
    }
    
    
    // MARK: - Decoding
    func decodeDataToArray() throws -> [[String:Any]] {
        let dictionary = try decodeDataToDictionary()
        
        guard let array = dictionary["items"] as? [[String:Any]] else {
            throw errorParser.getErrorWithLocalizedDescription("Request returned data which cannot be casted to array of given type.")
        }
        
        return array
    }
    
    
    func decodeDataToDictionary() throws -> [String:Any] {
        let anyJson = try JSONSerialization.jsonObject(with: data, options: [])
        
        guard let dictionary = anyJson as? [String:Any] else {
            throw errorParser.getErrorWithLocalizedDescription("Request returned data which cannot be casted to given type.")
        }
        
        return dictionary
    }
}
