//
//  ParametersEncoder.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

class ParametersEncoder {
    
    // MARK: - Variables
    private(set) var baseUrlString: String
    private(set) var parameters: [Parameter]
    
    
    // MARK: - Initializer
    init(baseUrlString: String, parameters: [Parameter]) {
        self.baseUrlString = baseUrlString
        self.parameters = parameters
    }
    
    
    // MARK: - Encoding
    func encodeUrl() -> URL? {
        guard var urlComponents = URLComponents(string: baseUrlString) else {
            return nil
        }
        
        urlComponents.queryItems = parameters.map { URLQueryItem(name: $0.name, value: $0.value) }
        
        return urlComponents.url
    }
}
