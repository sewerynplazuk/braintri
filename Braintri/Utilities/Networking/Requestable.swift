//
//  Requestable.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

protocol Requestable: class { }

extension Requestable {
    func configureGetRequest(_ request: inout URLRequest) {
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        #error("You must provide your authorization token for Github API")
        // Details what you need to do you can find here - https://developer.github.com/v3/auth/#via-oauth-tokens
        request.addValue("", forHTTPHeaderField: "")
    }
    
    
    func validateStatusCode(_ statusCode: Int) -> Bool {
        return 200...399 ~= statusCode
    }
}
