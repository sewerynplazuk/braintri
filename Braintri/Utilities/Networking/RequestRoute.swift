//
//  RequestRoute.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

enum RequestRoute {
    case searchRepositories(searchQuery: String, page: Int?)
    
    // MARK: - Variables
    var url: URL {
        switch self {
        case .searchRepositories(let searchQuery, let page):
            guard let url = URL(string: "https://api.github.com/search/repositories") else {
                fatalError("URL mustn't be nil")
            }
            
            var parameters: [Parameter] = [
                Parameter(name: "q", value: searchQuery)
            ]
            
            if let page = page {
                let pageParameter = Parameter(name: "page", value: "\(page)")
                parameters.append(pageParameter)
            }
            
            let parametersEncoder = ParametersEncoder(baseUrlString: url.absoluteString, parameters: parameters)
            if let encodedUrl = parametersEncoder.encodeUrl() {
                return encodedUrl
            } else {
                return url
            }
        }
    }
}
