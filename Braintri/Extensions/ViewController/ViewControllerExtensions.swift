//
//  ViewControllerExtensions.swift
//  Braintri
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

extension UIViewController {
    func addHorizontalMarginsConstraintsForView(_ view: UIView, marginWidth: CGFloat) {
        view.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: marginWidth).isActive = true
        view.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -marginWidth).isActive = true
    }
    
    
    func setRootViewBackgroundColor(_ backgroundColor: UIColor) {
        self.view.backgroundColor = backgroundColor
    }
}
