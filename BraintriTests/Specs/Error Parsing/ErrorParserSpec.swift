//
//  ErrorParserSpec.swift
//  BraintriTests
//
//  Created by Seweryn Plażuk on 07/12/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import Braintri

class ErrorParserSpec: QuickSpec {
    
    
    // MARK: - Spec
    override func spec() {
        self.initializationSpec()
        self.getErrorWithLocalizedDescriptionSpec()
    }
    
    
    private func initializationSpec() {
        context("After initialization") {
            it("Should has default domain and default code if those are not specified", closure: {
                let expectedDomain = "Braintri"
                let expectedCode = 0
                let expectedLocalizedDescription = "Localized Description"
                
                let errorParser = ErrorParser()
                let error = errorParser.getErrorWithLocalizedDescription("Localized Description")
                
                expect(error.localizedDescription).to(equal(expectedLocalizedDescription))
                
                expect((error as NSError).code).to(equal(expectedCode))
                expect(errorParser.code).to(equal(expectedCode))
                
                expect((error as NSError).domain).to(equal(expectedDomain))
                expect(errorParser.domain).to(equal(expectedDomain))
            })
        }
    }
    
    
    private func getErrorWithLocalizedDescriptionSpec() {
        context("After calling getErrorWithLocalizedDescription method") {
            it("Should return error with domain and code specified in the initializer", closure: {
                let expectedDomain = "ErrorParserSpec"
                let expectedCode = 401
                let expectedLocalizedDescription = "Localized Description"
                
                let errorParser = ErrorParser(domain: "ErrorParserSpec", code: 401)
                let error = errorParser.getErrorWithLocalizedDescription("Localized Description")
                
                expect(error.localizedDescription).to(equal(expectedLocalizedDescription))
                
                expect((error as NSError).code).to(equal(expectedCode))
                expect(errorParser.code).to(equal(expectedCode))
                
                expect((error as NSError).domain).to(equal(expectedDomain))
                expect(errorParser.domain).to(equal(expectedDomain))
            })
        }
    }
}
